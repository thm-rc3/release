package rc3.rssa;

import rc3.lib.optimization.AvailableOptimization;
import rc3.lib.optimization.Optimization;
import rc3.lib.optimization.OptimizationState;
import rc3.lib.utils.SetOperations;
import rc3.rssa.blocks.CallDependencyGraph;
import rc3.rssa.instances.Program;

import java.util.Collection;
import java.util.Set;

public final class UnusedProcedureElimination extends Optimization {
    private final Program originalProgram;

    public UnusedProcedureElimination(OptimizationState optimizationState, Program program) {
        super(AvailableOptimization.UNUSED_PROCEDURES_ELIMINATION, optimizationState);
        this.originalProgram = program;
    }

    public void performOptimization() {
        Collection<String> unusedProcedures = unusedProcedureNames();
        unusedProcedures.forEach(proc -> originalProgram.procedures().remove(proc));
        debug("Removed %d procedures from the RSSA program: %s",
                unusedProcedures.size(), unusedProcedures);
    }

    private Set<String> unusedProcedureNames() {
        final Set<String> allProcedures = originalProgram.procedureNames();
        final Set<String> usedProcedures = CallDependencyGraph.constructGraph(originalProgram)
                .getAllUsedProcedures();

        return SetOperations.difference(allProcedures, usedProcedures);
    }
}
