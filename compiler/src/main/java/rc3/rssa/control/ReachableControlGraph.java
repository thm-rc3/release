package rc3.rssa.control;

import rc3.januscompiler.Direction;
import rc3.lib.DirectionVar;
import rc3.rssa.blocks.BasicBlock;
import rc3.rssa.blocks.ControlGraph;
import rc3.rssa.instances.BinaryOperand;
import rc3.rssa.instances.ConditionalEntry;
import rc3.rssa.instances.ConditionalExit;
import rc3.rssa.instances.ControlInstruction;

import java.util.*;
import java.util.function.Function;

import static java.util.Collections.emptySet;

/**
 * A wrapper class for {@link ControlGraph} that statically analyses
 * {@link ControlInstruction.Conditional#getCondition() the condition
 * of conditional entry and exit points} in order to statically trace
 * possible control flow.
 * <p>
 * The utility method {@link ReachableControlGraph#computeReachable(Direction)}
 * provides broader access using the specialized predecessor and successor
 * operations to collect all reachable labels into a single {@link Set}.
 *
 * @implSpec This class partly implements optimization techniques described
 * in "Optimization of Reversible Control Flow Graphs".
 * @see BinaryOperand#compareConstant() statically evaluating conditions.
 */
public record ReachableControlGraph(ControlGraph graph) {

    private Set<String> reachableLabels(Direction direction, BasicBlock block) {
        return switch (block.getExit(direction)) {
            case ControlInstruction.Conditional conditional -> {
                final var constantCondition = conditional.getCondition().compareConstant();
                if (constantCondition.isPresent()) {
                    yield Set.of(conditional.getLabel(constantCondition.get()));
                } else {
                    yield Set.of(conditional.trueLabel(), conditional.falseLabel());
                }
            }
            case ControlInstruction.Unconditional unconditional -> Set.of(unconditional.label());
            default -> emptySet();
        };
    }

    private Set<String> fw(BasicBlock block) {
        return reachableLabels(Direction.FORWARD, block);
    }

    private Set<String> bw(BasicBlock block) {
        return reachableLabels(Direction.BACKWARD, block);
    }

    /**
     * Implementation of both <i>ComputeR<sub>fw</sub></i> and <i>ComputeR<sub>bw</sub></i>.
     */
    public Set<String> computeReachable(Direction direction) {
        final Function<BasicBlock, Set<String>> stepLabels = direction.choose(this::fw, this::bw);
        final BasicBlock origin = direction.chooseLazy(graph::getBegin, graph::getEnd);

        // Called Rfw or Rbw depending on direction.
        final Set<String> reachableLabels = new HashSet<>();
        // Called q in paper.
        final Queue<String> worklist = new ArrayDeque<>(stepLabels.apply(origin));

        while (!worklist.isEmpty()) {
            // Called l in paper.
            final String label = worklist.poll();

            if (reachableLabels.add(label)) {
                reachableLabels.addAll(
                        stepLabels.apply(graph.getByLabel(label, direction)));
            }
        }

        return reachableLabels;
    }

    public String toString() {
        final DirectionVar<Set<String>> reachable = DirectionVar.instantiate(this::computeReachable);
        final StringBuilder sb = new StringBuilder();
        sb.append("digraph { ");

        for (BasicBlock node : graph.getNodes()) {
            final boolean topIsConditional = node.entryPoint() instanceof ConditionalEntry;
            final boolean botIsConditional = node.exitPoint() instanceof ConditionalExit;
            final String shape;
            if (topIsConditional && botIsConditional) shape = "diamond";
            else if (topIsConditional) shape = "house";
            else if (botIsConditional) shape = "invhouse";
            else shape = "rectangle";

            final boolean isBeginOrEnd = node == graph.getBegin() || node == graph.getEnd();

            sb.append(node.graphId())
                    .append("[label=\"\"") // No text, just show the shape.
                    .append(",shape=").append(shape)
                    .append(",peripheries=").append(isBeginOrEnd ? 2 : 1)
                    .append("]; ");
        }

        for (String label : graph.getLabels()) {
            final DirectionVar<Boolean> isReachable = reachable.map(set -> set.contains(label));
            final String color;
            if (isReachable.get(Direction.FORWARD) && isReachable.get(Direction.BACKWARD)) color = "green";
            else if (isReachable.get(Direction.FORWARD)) color = "blue";
            else if (isReachable.get(Direction.BACKWARD)) color = "yellow";
            else color = "black";

            sb.append(graph.getByLabel(label, Direction.BACKWARD).graphId())
                    .append(" -> ")
                    .append(graph.getByLabel(label, Direction.FORWARD).graphId())
                    .append("[label=\"").append(label).append("\"")
                    .append("color=").append(color)
                    .append("];");
        }
        sb.append(" }");
        return sb.toString();
    }
}
