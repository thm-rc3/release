package rc3.rssa.control;

import rc3.lib.DirectionVar;
import rc3.lib.messages.WarningMessage;
import rc3.lib.optimization.AvailableOptimization;
import rc3.lib.optimization.Optimization;
import rc3.lib.optimization.OptimizationState;
import rc3.lib.utils.SetOperations;
import rc3.rssa.blocks.BasicBlock;
import rc3.rssa.blocks.CallDependencyGraph;
import rc3.rssa.blocks.ControlGraph;
import rc3.rssa.instances.*;

import java.util.*;

import static rc3.lib.utils.CollectionUtils.anyElement;
import static rc3.lib.utils.CollectionUtils.updateAtIndex;
import static rc3.lib.utils.SetOperations.*;

/**
 * This optimization implements the removal of unreachable code.
 *
 * @implSpec Deworetzki, Niklas and Gail, Lukas. <i>Optimization of Reversible
 * Control Flow Graphs</i>. In: 15th Conference on Reversible Computation
 * (see Section 3.1 for the strict variant and 3.2 for the non-strict variant).
 */
public final class RemoveUnreachableCode extends Optimization {
    private final Strictness strictness;

    public RemoveUnreachableCode(OptimizationState optimizationState) {
        this(optimizationState, parseStrictness(optimizationState));
    }

    public RemoveUnreachableCode(OptimizationState optimizationState, Strictness strictness) {
        super(AvailableOptimization.ELIMINATE_UNREACHABLE, optimizationState);
        this.strictness = strictness;
    }

    public enum Strictness {
        /**
         * Only remove code that is unreachable in both execution direction.
         */
        STRICT,
        /**
         * Consider code paths within a routine that lead to a reversibility violation
         * as unreachable and remove them.
         */
        PROPAGATE_INTRAPROCEDURAL,
        /**
         * Consider calls to degenerate routines as additional causes for reversibility violations
         * and consider them as unreachable. Also remove code paths that cause reversibility violations
         * via control flow.
         */
        PROPAGATE_INTERPROCEDURAL
    }

    private static Strictness parseStrictness(OptimizationState state) {
        final List<String> parameters = state.getOptimizationParameters(AvailableOptimization.ELIMINATE_UNREACHABLE);

        for (int i = parameters.size() - 1; i >= 0; i--) {
            switch (parameters.get(i).toLowerCase()) {
                case "strict":
                    return Strictness.STRICT;

                case "intra":
                case "intraprocedural":
                    return Strictness.PROPAGATE_INTRAPROCEDURAL;

                case "inter":
                case "interprocedural":
                    return Strictness.PROPAGATE_INTERPROCEDURAL;
            }
        }

        // Stay strict when strictness was not defined explicitly and only safe optimizations are enabled.
        return state.getOptimizationLevel() > 1
                ? Strictness.PROPAGATE_INTRAPROCEDURAL
                : Strictness.STRICT;
    }

    /**
     * Remove unreachable code from all routines of a program.
     */
    public void optimizeProgram(Program program) {
        new ProgramOptimizer(program).optimizeProgram();
    }


    private class ProgramOptimizer {
        private final Queue<ControlGraph> queuedRoutines = new ArrayDeque<>();
        private final Program program;

        private final Set<String> degenerateRoutines = new HashSet<>();
        private final CallDependencyGraph dependencyGraph;

        public ProgramOptimizer(Program program) {
            this.program = program;
            this.dependencyGraph = CallDependencyGraph.constructGraph(program);
        }

        public void optimizeProgram() {
            queuedRoutines.addAll(program.procedures().values());
            while (!queuedRoutines.isEmpty()) {
                final var routine = queuedRoutines.poll();
                debug("Removing unreachable code from routine %s.", routine.getProcedureName());

                // Don't re-consider a degenerate routine for optimization.
                if (!degenerateRoutines.contains(routine.getProcedureName())) {
                    optimizeRoutine(queuedRoutines.poll());
                }
            }

            if (!degenerateRoutines.isEmpty()) {
                reportWarningsForDegenerateRoutines();
            }
        }

        private void reportWarningsForDegenerateRoutines() {
            final List<String> routineNames = new ArrayList<>(degenerateRoutines);
            Collections.sort(routineNames);

            for (String routineName : routineNames) {
                new WarningMessage.DegenerateRoutine(routineName)
                        .report();
            }
        }

        private void optimizeRoutine(ControlGraph graph) {
            final Set<String> reachableLabels = getReachableLabelsForOptimization(graph);
            debug("Reachable labels in routine %s: %s", graph.getProcedureName(), reachableLabels);
            final Set<BasicBlock> reachableCode = new RoutineOptimizer(reachableLabels).removeUnreachable(graph);

            // An empty set means that the routine degenerated.
            if (!reachableCode.isEmpty()) {
                graph.updateNodes(reachableCode);
            } else {
                degenerateRoutines.add(graph.getProcedureName());
                if (strictness == Strictness.PROPAGATE_INTERPROCEDURAL) {
                    enqueueCallers(graph.getProcedureName());
                }
            }
        }

        private void enqueueCallers(String unreachableRoutineName) {
            final var callers = dependencyGraph.getCallingProcedures(unreachableRoutineName);
            debug("Detected degenerate routine %s. Callers will be analysed again: %s.",
                    unreachableRoutineName, callers);

            for (String caller : callers) {
                final var callerRoutine = program.getProcedure(caller);
                if (!queuedRoutines.contains(callerRoutine)) { // Only enqueue routines not already enqueued to prevent infinite loops.
                    queuedRoutines.add(callerRoutine);
                }
            }
        }

        private Set<String> getReachableLabelsForOptimization(ControlGraph graph) {
            final var reachable = DirectionVar.instantiate(new ReachableControlGraph(graph)::computeReachable);
            if (strictness == Strictness.STRICT) {
                return reachable.fold(SetOperations::intersection);
            } else {
                return fixReachable(reachable.fold(SetOperations::union), graph);
            }
        }

        /**
         * Implements <i>Fix<sub>R</sub></i> from paper.
         *
         * @return A {@link Set} of reachable labels, where reversibility violations are
         * used to infer that code paths causing a violation must be unreachable.
         */
        private Set<String> fixReachable(Set<String> reachable, ControlGraph graph) {
            int reachableSize;
            do {
                reachableSize = reachable.size();

                for (BasicBlock block : graph) {
                    if (block != graph.getBegin() && !intersects(block.entryLabels(), reachable)) {
                        reachable.removeAll(block.exitLabels());
                    }
                    if (block != graph.getEnd() && !intersects(block.exitLabels(), reachable)) {
                        reachable.removeAll(block.entryLabels());
                    }

                    // Calls to degenerate routines allow us to infer that blocks containing these calls must be unreachable.
                    if (strictness == Strictness.PROPAGATE_INTERPROCEDURAL && containsUnreachableCall(block)) {
                        reachable.removeAll(block.entryLabels());
                        reachable.removeAll(block.exitLabels());
                    }
                }
            } while (reachable.size() < reachableSize); // Repeat until no more changes occur.
            return reachable;
        }

        private boolean containsUnreachableCall(BasicBlock block) {
            for (Instruction instruction : block) {
                if (instruction instanceof CallInstruction callInstruction &&
                        degenerateRoutines.contains(callInstruction.procedure)) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * Implements removal of unreachable code within a routine.
     */
    private record RoutineOptimizer(Set<String> reachableLabels) {

        /**
         * Implements <i>RemoveUnreachable</i> from paper.
         *
         * @return A {@link Set} of blocks that are considered reachable.
         */
        private Set<BasicBlock> removeUnreachable(ControlGraph graph) {
            final Set<BasicBlock> preservedBlocks = new HashSet<>(); // Called p in paper.
            for (BasicBlock block : graph) {
                if (intersects(union(block.entryLabels(), block.exitLabels()), reachableLabels)) {
                    updateBlock(block);
                    preservedBlocks.add(block);
                }
            }
            return preservedBlocks;
        }

        private void updateBlock(BasicBlock block) {
            final List<Instruction> instructions = block.getInstructions();
            updateAtIndex(instructions, 0, this::update);
            updateAtIndex(instructions, instructions.size() - 1, this::update);
        }

        /**
         * Implements <i>update</i> from paper.
         *
         * @return Instruction without references to unreachable labels.
         */
        private Instruction update(Instruction ins) {
            if (ins instanceof ControlInstruction control &&
                    !reachableLabels.containsAll(control.associatedLabels())) {
                return switch (ins) {
                    case ConditionalEntry entry -> new UnconditionalEntry(getRemainingReachableLabel(entry))
                            .withParameters(control.getParameters());
                    case ConditionalExit exit -> new UnconditionalExit(getRemainingReachableLabel(exit))
                            .withParameters(control.getParameters());
                    default -> ins;
                };
            }
            return ins;
        }

        private String getRemainingReachableLabel(ControlInstruction instruction) {
            final Set<String> associatedLabels = intersection(instruction.associatedLabels(), reachableLabels);
            assert associatedLabels.size() == 1;
            return anyElement(associatedLabels);
        }
    }
}
