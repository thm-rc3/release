package rc3.rssa.control;

import rc3.januscompiler.Direction;
import rc3.lib.optimization.AvailableOptimization;
import rc3.lib.optimization.OptimizationState;
import rc3.rssa.blocks.BasicBlock;
import rc3.rssa.blocks.ControlGraph;
import rc3.rssa.instances.*;

import java.util.*;
import java.util.stream.Collectors;

import static rc3.lib.utils.SetOperations.intersects;

/**
 * If an empty block starts with an unconditional entry and ends with an unconditional
 * exit, it only serves as <i>glue</i> to merge two other labels. Removing the
 * empty block and connecting the surrounding labels simplifies the structure of
 * a {@link ControlGraph}.
 */
public class EliminateEmptyBlocks implements ControlGraphSimplification.SimplificationAlgorithm {
    OptimizationState optimizationState;

    EliminateEmptyBlocks(OptimizationState optimizationState) {
        this.optimizationState = optimizationState;
    }

    @Override
    public boolean isApplicable(ControlGraph graph, BasicBlock block) {

        if (optimizationState.isOptimizationEnabled(AvailableOptimization.ENABLE_UNSAFE_OPTIMIZATIONS)) {
            return containsNoInstructions(block) &&
                    parameterListsContainOnlyVariables(block) &&
                    parameterListsAreTheSame(block) &&
                    (controlPointsAreNotConditional(block) || controlPointConditionsCorrelate(block));
        } else {
            return containsNoInstructions(block) &&
                    controlPointsAreNotConditional(block) &&
                    parameterListsContainOnlyVariables(block) &&
                    parameterListsAreTheSame(block) &&
                    isNotConnectedToItself(block);
        }
    }

    private boolean containsNoInstructions(BasicBlock block) {
        return block.getInstructions().size() == 2;
    }

    private boolean controlPointsAreNotConditional(BasicBlock block) {
        return block.entryPoint().isUnconditional() &&
                block.exitPoint().isUnconditional();
    }

    /**
     * Checks whether to conditions correlate, i.e. whether the value of the second condition can be determined by just looking at the first condition.
     *
     * @param a The first condition
     * @param b The second condition
     *
     * @return And empty optional if the conditions don't correlate. True if they correlate and evaluate to the same value, False if they correlate and evaluate to the opposite value.
     */
    public static Optional<Boolean> conditionCorrelation(BinaryOperand a, BinaryOperand b) {
        if (a.equals(b) || a.flippedComparison().equals(b)) return Optional.of(true);

        final var b_inverted = b.invertedComparison();
        if (a.equals(b_inverted) || a.flippedComparison().equals(b_inverted)) return Optional.of(false);

        return Optional.empty();
    }

    private boolean controlPointConditionsCorrelate(BasicBlock block) {
        return block.entryPoint().isConditional() &&
                block.exitPoint().isConditional() &&
                conditionCorrelation(((ConditionalEntry) block.entryPoint()).getCondition(), ((ConditionalExit) block.exitPoint()).getCondition()).isPresent();
    }

    private boolean parameterListsContainOnlyVariables(BasicBlock block) {
        return block.entryPoint().getParameters().stream().allMatch(Variable.class::isInstance) &&
                block.exitPoint().getParameters().stream().allMatch(Variable.class::isInstance);
    }

    private boolean parameterListsAreTheSame(BasicBlock block) {
        return Objects.equals(block.entryPoint().getParameters(), block.exitPoint().getParameters());
    }

    private boolean isNotConnectedToItself(BasicBlock block) {
        return !intersects(block.entryLabels(), block.exitLabels());
    }

    @Override
    public Set<BasicBlock> apply(ControlGraph graph, BasicBlock emptyBlock) {
        // This method implements both the aggressive and the strict version

        if (emptyBlock.exitPoint().isConditional()) {
            final var entry = ((ConditionalEntry) emptyBlock.entryPoint());
            final var exit = ((ConditionalExit) emptyBlock.exitPoint());

            // If we get here, the conditions MUST correlate, so we can get the correlation.
            final var correlation = conditionCorrelation(entry.getCondition(), exit.getCondition()).orElseThrow();

            updateEntryPoint(graph.getByLabel(exit.trueLabel, Direction.FORWARD), exit.trueLabel, entry.getLabel(correlation));
            updateEntryPoint(graph.getByLabel(exit.falseLabel, Direction.FORWARD), exit.falseLabel, entry.getLabel(!correlation));
        } else {
            final var entry = ((UnconditionalEntry) emptyBlock.entryPoint());
            final var exit = ((UnconditionalExit) emptyBlock.exitPoint());

            updateEntryPoint(graph.getByLabel(exit.label, Direction.FORWARD), exit.label, entry.label);
        }

        graph.getNodes().remove(emptyBlock);

        return emptyBlock.exitLabels().stream().map(l -> graph.byLabel(l, Direction.FORWARD).orElseThrow()).collect(Collectors.toSet());
    }

    private void updateEntryPoint(BasicBlock block, String originalLabel, String newLabel) {
        final var instruction = block.entryPoint();
        if (instruction instanceof UnconditionalEntry entry && entry.label.equals(originalLabel)) {
            entry.label = newLabel;

        } else if (instruction instanceof ConditionalEntry entry) {
            if (entry.trueLabel.equals(originalLabel)) {
                entry.trueLabel = newLabel;
            } else if (entry.falseLabel.equals(originalLabel)) {
                entry.falseLabel = newLabel;
            }
        }
    }
}
