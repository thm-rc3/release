package rc3.rssa.control;

import rc3.lib.optimization.AvailableOptimization;
import rc3.lib.optimization.OptimizationState;
import rc3.rssa.blocks.BasicBlock;
import rc3.rssa.blocks.ControlGraph;

import java.util.Set;

import static rc3.lib.utils.CollectionUtils.anyElement;
import static rc3.lib.utils.SetOperations.intersects;

/**
 * If an unconditional exit leads to an unconditional entry, both {@link BasicBlock blocks}
 * are always executed after one another. This {@link ControlGraphSimplification.SimplificationAlgorithm} merges such
 * blocks, removing the redundant jump performed by exiting one block and beginning a new one.
 */
public class MergeStrictlyConsecutiveBlocks implements ControlGraphSimplification.SimplificationAlgorithm {
    OptimizationState optimizationState;

    MergeStrictlyConsecutiveBlocks(OptimizationState optimizationState) {
        this.optimizationState = optimizationState;
    }

    @Override
    public boolean isApplicable(ControlGraph graph, BasicBlock block) {
        if (optimizationState.isOptimizationEnabled(AvailableOptimization.ENABLE_UNSAFE_OPTIMIZATIONS)) {
            //TODO: Refactor this to a separate flag?

            // Aggressive variant where blocks are strictly consecutive even if they are connected by two labels
            var successors = graph.getSuccessors(block);

            return successors.stream().distinct().count() == 1 &&
                    isNotOwnSuccessor(block);
        } else {
            return hasUnconditionalExit(block) &&
                    hasUnconditionalEntry(anyElement(graph.getSuccessors(block))) &&
                    isNotOwnSuccessor(block);
        }
    }

    private boolean hasUnconditionalExit(BasicBlock block) {
        return block.exitPoint().isUnconditional();
    }

    private boolean hasUnconditionalEntry(BasicBlock block) {
        return block.entryPoint().isUnconditional();
    }

    private boolean isNotOwnSuccessor(BasicBlock block) {
        return !intersects(block.entryLabels(), block.exitLabels());
    }

    @Override
    public Set<BasicBlock> apply(ControlGraph graph, BasicBlock block) {

        final var joined = graph.join(block, anyElement(graph.getSuccessors(block)));
        return Set.of(joined);
    }
}
