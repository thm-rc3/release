package rc3.rssa;

import rc3.lib.optimization.AvailableOptimization;
import rc3.lib.optimization.OptimizationState;
import rc3.rssa.control.ControlGraphSimplification;
import rc3.rssa.control.RemoveUnreachableCode;
import rc3.rssa.cp.GlobalConstantPropagation;
import rc3.rssa.cp.LocalConstantPropagation;
import rc3.rssa.cse.CommonSubexpressionElimination;
import rc3.rssa.dce.DeadCodeElimination;
import rc3.rssa.inlining.Inlining;
import rc3.rssa.instances.Program;
import rc3.rssa.used.LiveVariables;
import rc3.rssa.used.UsedVariables;

import java.util.function.Function;

/**
 * Optimizes a given RSSA {@link Program}.
 */
public record RSSAOptimizer(OptimizationState optimizationState) implements Function<Program, Program> {
    // TODO: Optimization order

    @Override
    public Program apply(Program rssaProgram) {
        if (optimizationState.isOptimizationEnabled(AvailableOptimization.INLINING)) {
            rssaProgram = new Inlining(optimizationState, rssaProgram).performOptimization();
        }

        if (optimizationState.isOptimizationEnabled(AvailableOptimization.SIMPLIFY_CONTROL_FLOW)) {
            rssaProgram.applyOptimization(new ControlGraphSimplification(optimizationState));
        }

        if (optimizationState.isOptimizationEnabled(AvailableOptimization.COMMON_SUBEXPRESSION_ELIMINATION)) {
            rssaProgram.applyOptimization(new CommonSubexpressionElimination(optimizationState, rssaProgram));
        }

        if (optimizationState.isOptimizationEnabled(AvailableOptimization.GLOBAL_CONSTANT_PROPAGATION)) {
            rssaProgram.applyOptimization(new GlobalConstantPropagation(optimizationState));
        } else if (optimizationState.isOptimizationEnabled(AvailableOptimization.LOCAL_CONSTANT_PROPAGATION)) {
            rssaProgram.applyOptimization(new LocalConstantPropagation(optimizationState));
        }

        if (optimizationState.isOptimizationEnabled(AvailableOptimization.ELIMINATE_UNREACHABLE)) {
            new RemoveUnreachableCode(optimizationState).optimizeProgram(rssaProgram);
        }

        if (optimizationState.isOptimizationEnabled(AvailableOptimization.SIMPLIFY_CONTROL_FLOW)) {
            rssaProgram.applyOptimization(new ControlGraphSimplification(optimizationState));
        }

        if (optimizationState.isOptimizationEnabled(AvailableOptimization.LIVE_VARIABLES)) {
            LiveVariables.dumpLiveVariables(rssaProgram);
        }
        if (optimizationState.isOptimizationEnabled(AvailableOptimization.USED_VARIABLES)) {
            UsedVariables.dumpLiveVariables(rssaProgram);
        }

        if (optimizationState.isOptimizationEnabled(AvailableOptimization.DEAD_CODE_ELIMINATION)) {
            rssaProgram.applyOptimization(new DeadCodeElimination(optimizationState));
        }

        if (optimizationState.isOptimizationEnabled(AvailableOptimization.UNUSED_PROCEDURES_ELIMINATION)) {
            new UnusedProcedureElimination(optimizationState, rssaProgram).performOptimization();
        }

        return rssaProgram;
    }
}
