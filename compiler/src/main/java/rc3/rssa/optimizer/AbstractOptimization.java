package rc3.rssa.optimizer;

import rc3.rssa.blocks.BasicBlock;
import rc3.rssa.blocks.ControlGraph;
import rc3.rssa.instances.Program;

public abstract class AbstractOptimization implements Optimization {
    @Override
    public void apply(Scheduler scheduler, Program program) {
        program.procedures().values().forEach(p -> this.apply(scheduler, p));
    }

    @Override
    public void apply(Scheduler scheduler, ControlGraph procedure) {
        procedure.getNodes().forEach(n -> this.apply(scheduler, n));
    }

    @Override
    public void apply(Scheduler scheduler, BasicBlock block) {
        throw new UnsupportedOperationException("");
    }
}