package rc3.rssa.optimizer;

import rc3.rssa.blocks.BasicBlock;
import rc3.rssa.blocks.ControlGraph;
import rc3.rssa.instances.Program;

interface Optimization {
    void apply(Scheduler scheduler, Program program);

    void apply(Scheduler scheduler, ControlGraph procedure);

    void apply(Scheduler scheduler, BasicBlock block);
}