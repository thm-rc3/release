package rc3.rssa.optimizer.optimizations;

import rc3.rssa.blocks.BasicBlock;
import rc3.rssa.blocks.ControlGraph;
import rc3.rssa.instances.*;
import rc3.rssa.optimizer.AbstractOptimization;
import rc3.rssa.optimizer.Configuration;
import rc3.rssa.optimizer.Scheduler;

import java.util.*;
import java.util.function.Function;

public class RemoveEmptyBlocks extends AbstractOptimization {

    Configuration configuration;

    public RemoveEmptyBlocks(Configuration configuration) {
        this.configuration = configuration;
    }

    public static Optional<Boolean> inferBijection(BinaryOperand a, BinaryOperand b) {
        if (a.equals(b) || a.flippedComparison().equals(b)) return Optional.of(true);

        final var b_inverted = b.invertedComparison();
        if (a.equals(b_inverted) || a.flippedComparison().equals(b_inverted)) return Optional.of(false);

        return Optional.empty();
    }

    private Map<String, String> getRedirections(BasicBlock block) {
        if (block.getInstructions().size() == 2 &&          // No instructions beyond entry and exit
                block.entryPoint().getParameters().equals(block.exitPoint().getParameters()) && // parameter lists are the same
                block.entryPoint().getParameters().stream().allMatch(Variable.class::isInstance)  // All parameters are variables
        ) {
            if (block.entryPoint() instanceof UnconditionalEntry entry && block.exitPoint() instanceof UnconditionalExit exit) {
                // Entry and exit are unconditional, the block is definitely a redirecting block
                return Map.of(entry.label, exit.label);
            }

            if (configuration.ASSERTION_FAILURES_ARE_UNDEFINED &&
                    block.entryPoint() instanceof ConditionalEntry entry && block.exitPoint() instanceof ConditionalExit exit) {
                // Entry and exit are conditional. The block is a redirecting block if
                //  1. Reversibility violations are undefined/unreachable
                //  2. There is an inferable bijection between the conditions.

                final var correlation = inferBijection(entry.getCondition(), exit.getCondition());

                if (correlation.isPresent()) {
                    // There is a bijection. USe it to construct redirections.
                    return Map.of(
                            entry.trueLabel, exit.getLabel(correlation.get()),
                            entry.falseLabel, exit.getLabel(!correlation.get()));
                }
            }
        }

        return Map.of();

    }

    @Override
    public void apply(Scheduler scheduler, ControlGraph procedure) {
        final var oldBlockCount = procedure.getNodes().size();

        // Make a copy of the blocks so we can easily remove empty blocks
        final var blocks = procedure.getNodes();

        procedure.getNodes().stream().iterator().forEachRemaining(a -> {
            final var redirections = getRedirections(a);

            final Function<String, String> updateLabel = l -> redirections.getOrDefault(l, l);
            final Function<Instruction, Instruction> update = i -> switch(i) {
                case UnconditionalEntry entry -> new UnconditionalEntry(updateLabel.apply(entry.label));
                case UnconditionalExit exit -> new UnconditionalExit(updateLabel.apply(exit.label));
                case ConditionalEntry entry -> new ConditionalEntry(updateLabel.apply(entry.trueLabel), updateLabel.apply(entry.falseLabel), entry.getCondition());
                case ConditionalExit exit -> new ConditionalExit(updateLabel.apply(exit.trueLabel), updateLabel.apply(exit.falseLabel), exit.getCondition());
                default -> throw new IllegalStateException("Unexpected value: " + i);
            };

            if (!redirections.isEmpty()) {
                // The block defines redirections and is therefore empty. Apply them now.
                // We can either update the exit of the predecessor or the entry of the successor blocks
                // We do the former, as this is the order in which the redirections are placed in the map.

                blocks.remove(a);

                procedure.getPredecessors(a).forEach(pre -> pre.updateExit(update));
            }
        });

        if (procedure.getNodes().size() != oldBlockCount) {
            // TODO: Schedule optimizations
        }

        super.apply(scheduler, procedure);
    }
}
