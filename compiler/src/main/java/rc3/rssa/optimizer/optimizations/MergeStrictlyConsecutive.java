package rc3.rssa.optimizer.optimizations;

import rc3.rssa.VariableRenamer;
import rc3.rssa.blocks.BasicBlock;
import rc3.rssa.blocks.ControlGraph;
import rc3.rssa.instances.*;
import rc3.rssa.optimizer.AbstractOptimization;
import rc3.rssa.optimizer.Configuration;
import rc3.rssa.optimizer.Scheduler;

import java.util.*;
import java.util.stream.Collectors;

import static rc3.lib.utils.CollectionUtils.combineWith;
import static rc3.lib.utils.CollectionUtils.subList;

public class MergeStrictlyConsecutive extends AbstractOptimization {
    Configuration configuration;

    public MergeStrictlyConsecutive(Configuration configuration) {
        this.configuration = configuration;
    }

    private BasicBlock merge(ControlGraph graph, BasicBlock predecessor, BasicBlock successor) {
        final ControlInstruction exitPoint = predecessor.exitPoint();
        final ControlInstruction entryPoint = successor.entryPoint();

        final List<Instruction> requiredAssignments = new ArrayList<>();
        final Map<Variable, Atom> requiredRenames = new HashMap<>();

        combineWith(exitPoint.getParameters(), entryPoint.getParameters(), (sourceAtom, targetAtom) -> {
            if (targetAtom instanceof Variable targetVariable) {
                requiredRenames.put(targetVariable, sourceAtom);
            } else if (sourceAtom instanceof Variable sourceVariable) {
                requiredRenames.put(sourceVariable, targetAtom);
            } else if (!Objects.equals(sourceAtom, targetAtom)) { // Both are different constants.
                requiredAssignments.add(ArithmeticAssignment.assignAndDestroy(targetAtom, sourceAtom));
            }
        });

        final var renamer = VariableRenamer.fromMap(requiredRenames);

        final List<Instruction> joinedInstructions = new ArrayList<>();
        joinedInstructions.addAll(subList(predecessor.getInstructions(), 0, 1));
        joinedInstructions.addAll(requiredAssignments);
        joinedInstructions.addAll(subList(successor.getInstructions(), 1, 0));

        joinedInstructions.forEach(renamer);

        final BasicBlock joinedBlock = new BasicBlock(predecessor.number);
        joinedBlock.setInstructions(joinedInstructions);

        List<BasicBlock> updatedNodes = graph.getNodes().stream().filter(b -> b != predecessor && b != successor).collect(Collectors.toList());
        updatedNodes.add(joinedBlock);

        graph.updateNodes(updatedNodes);

        return joinedBlock;
    }

    Optional<BasicBlock> getStrictlyConsecutiveSuccessor(ControlGraph graph, BasicBlock a) {
        final var successors = graph.getSuccessors(a);

        // If a block has more than one successor, they cannot be strictly consecutive
        if (successors.size() != 1) return Optional.empty();

        // Get the single successor
        final var b = successors.iterator().next();

        // Compare to paper.
        boolean isStrictlyConsecutive =
                a.exitLabels().equals(b.entryLabels()) &&
                        (configuration.ASSERTION_FAILURES_ARE_UNDEFINED || a.exitLabels().size() == 1);

        return isStrictlyConsecutive ? Optional.of(b) : Optional.empty();
    }

    @Override
    public void apply(Scheduler scheduler, ControlGraph procedure) {
        Set<BasicBlock> visited = new HashSet<>();
        Queue<BasicBlock> worklist = new ArrayDeque<>();

        worklist.add(procedure.getBegin());

        final var oldBlockCount = procedure.getNodes().size();

        /*
         * In the paper, strictly consecutive blocks are described in terms of pairs.
         * Visiting every pair of blocks is O(n^2), which is slower than it neads to be.
         * Instead, we visit every block once and check whether they have a strictly consecutive successor.
         * To avoid having to redo this after every merge, we visit the blocks in execution order in forwards direction with a worklist (breadth first).
         */
        while (!worklist.isEmpty()) {
            BasicBlock a = worklist.poll();

            if (visited.contains(a)) continue;
            visited.add(a);

            final var strictly_consecutive_successor = getStrictlyConsecutiveSuccessor(procedure, a);

            if (strictly_consecutive_successor.isPresent()) {
                final var merged = merge(procedure, a, strictly_consecutive_successor.get());

                // TODO: Schedule local optimizations

                worklist.add(merged);
            } else {
                worklist.addAll(procedure.getSuccessors(a));
            }
        }

        if (procedure.getNodes().size() != oldBlockCount) {
            // TODO: Schedule optimizations
        }

        super.apply(scheduler, procedure);
    }
}
