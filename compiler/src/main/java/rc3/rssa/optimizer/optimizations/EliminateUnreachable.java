package rc3.rssa.optimizer.optimizations;

import rc3.lib.DirectionVar;
import rc3.lib.utils.SetOperations;
import rc3.rssa.blocks.BasicBlock;
import rc3.rssa.blocks.ControlGraph;
import rc3.rssa.control.ReachableControlGraph;
import rc3.rssa.instances.ConditionalEntry;
import rc3.rssa.instances.ConditionalExit;
import rc3.rssa.instances.Instruction;
import rc3.rssa.instances.UnconditionalEntry;
import rc3.rssa.optimizer.AbstractOptimization;
import rc3.rssa.optimizer.Configuration;
import rc3.rssa.optimizer.Scheduler;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

public class EliminateUnreachable extends AbstractOptimization {
    Configuration configuration;

    public EliminateUnreachable(Configuration configuration) {
        this.configuration = configuration;
    }

    private Set<String> getReachableLabels(ControlGraph graph) {
        ReachableControlGraph reachable = new ReachableControlGraph(graph);

        final var reachables = DirectionVar.instantiate(reachable::computeReachable);

        if (configuration.ASSERTION_FAILURES_ARE_UNDEFINED) {
            final var R = reachables.fold(SetOperations::intersection);

            while (true) {
                var old_n = R.size();

                graph.getNodes().forEach(b -> {
                    if (b != graph.getBegin() && !SetOperations.intersects(b.entryLabels(), R))
                        R.removeAll(b.exitLabels());

                    if (b != graph.getEnd() && !SetOperations.intersects(b.exitLabels(), R))
                        R.removeAll(b.entryLabels());
                });

                if (old_n == R.size()) break;
            }

            return R;
        } else {
            return reachables.fold(SetOperations::union);
        }
    }

    private void removeUnreachable(Set<String> R, ControlGraph G) {

        // According to the update function in the paper
        final Function<Instruction, Instruction> update = (i ->
                switch (i) {
                    case ConditionalEntry ce && !R.contains(ce.trueLabel) -> new UnconditionalEntry(ce.falseLabel);
                    case ConditionalEntry ce && !R.contains(ce.falseLabel) -> new UnconditionalEntry(ce.trueLabel);
                    case ConditionalExit ce && !R.contains(ce.trueLabel) -> new UnconditionalEntry(ce.falseLabel);
                    case ConditionalExit ce && !R.contains(ce.falseLabel) -> new UnconditionalEntry(ce.trueLabel);
                    default -> i;
                });

        final var p = new HashSet<BasicBlock>();

        G.getNodes().forEach(b -> {
            if (SetOperations.intersects(SetOperations.union(b.entryLabels(), b.exitLabels()), R)) {
                p.add(b);

                b.updateEntry(update);
                b.updateExit(update);
            }
        });

        G.updateNodes(p);
    }

    @Override
    public void apply(Scheduler scheduler, ControlGraph procedure) {
        final var old_block_count = procedure.getNodes().size();

        final var R = getReachableLabels(procedure);

        removeUnreachable(R, procedure);

        if (procedure.getNodes().size() != old_block_count) {
            // TODO: Schedule optimizations
        }
    }
}
