package rc3.rssa.optimizer;

import rc3.lib.NotImplemented;
import rc3.rssa.instances.Program;

import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;

public class Scheduler {
    public final Configuration configuration;
    private final Queue<OptimizationJob> jobs = new ArrayDeque<>();

    public Scheduler(Configuration configuration) {
        this.configuration = configuration;
    }

    private static class OptimizationJob {
        OptimizationID optimization;
        boolean isGlobal;
        Set<String> procedure_targets = new HashSet<>();

        public OptimizationJob(OptimizationID id) {
            this.optimization = id;
            this.isGlobal = true;
        }

        public OptimizationJob(OptimizationID id, String procedure) {
            this.optimization = id;
            this.isGlobal = false;
            this.procedure_targets.add(procedure);
        }

        boolean incorporate(OptimizationJob other) {
            if (other.optimization == this.optimization) {
                this.isGlobal = this.isGlobal || other.isGlobal;
                this.procedure_targets.addAll(other.procedure_targets);
                return true;
            }

            return false;
        }

        void apply(Program program) {
            throw new NotImplemented();
        }
    }

    private void schedule(OptimizationJob job) {
        // TODO: Check whether this monstrosity even works
        if (this.jobs.stream().filter(j -> j.incorporate(job)).findFirst().isEmpty()) {
            this.jobs.add(job);
        }
    }

    public void schedule(OptimizationID id, String procedure) {
        this.schedule(new OptimizationJob(id, procedure));
    }

    public void schedule(OptimizationID id) {
        this.schedule(new OptimizationJob(id));
    }

    public void drive(Program program) {
        // TODO: Limit number of passes
        while (!jobs.isEmpty()) jobs.poll().apply(program);
    }
}