package rc3.januscompiler.syntax;

import rc3.januscompiler.syntax.visitor.VisitableTree;
import rc3.lib.parsing.Position;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This abstract class is the superclass of all nodes in the abstract syntax tree.
 * <p>
 * It implements the visitor pattern by implementing the {@link VisitableTree} interface.
 * </p>
 */
public sealed abstract class Tree implements VisitableTree
        permits Declaration, Expression, Program, Statement, Variable {
    public final Position position;

    protected Tree(Position position) {
        this.position = position;
    }

    /**
     * Creates a String representation for a list as member variable.
     *
     * @param name The name of the variable used in the representation.
     * @param list The list to create a string representation of.
     */
    protected static String formatMember(String name, List<?> list) {
        if (list.size() == 0) {
            return String.format("%s = []", name);
        } else {
            String listString = list.stream()
                    .map(String::valueOf)
                    .collect(Collectors.joining(",\n"));

            return String.format("%s = [\n%s\n]", name, indent(listString));
        }
    }

    /**
     * Creates a String representation for a member variable.
     *
     * @param name  The name of the variable used in the representation.
     * @param value The value of the member variable.
     */
    protected static String formatMember(String name, String value) {
        return String.format("%s = %s", name, value);
    }

    /**
     * Creates a String representation of a {@link Tree} node.
     * This method is used in the concrete subclasses of {@link Tree}.
     *
     * @param name     The name of the concrete subclass.
     * @param children The children present in a subclass' instance.
     */
    protected static String formatTree(String name, Object... children) {
        if (children.length == 0) {
            return String.format("%s()", name);
        } else {
            String memberString = Arrays.stream(children)
                    .map(String::valueOf)
                    .collect(Collectors.joining(",\n"));

            return String.format("%s(\n%s\n)", name, indent(memberString));
        }
    }

    /**
     * Prepends a TAB-character (\t) in front of every line of the given String.
     */
    private static String indent(String str) {
        return str.lines()
                .map(line -> "\t" + line)
                .collect(Collectors.joining("\n"));
    }
}
