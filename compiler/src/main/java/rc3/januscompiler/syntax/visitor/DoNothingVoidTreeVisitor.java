package rc3.januscompiler.syntax.visitor;

import rc3.januscompiler.syntax.*;

/**
 * This class implements every <code>visitVoid</code> method with an empty effect.
 */
public class DoNothingVoidTreeVisitor extends VoidTreeVisitor {

    @Override
    public void visitVoid(AssignStatement assignStatement) {

    }

    @Override
    public void visitVoid(BinaryExpression binaryExpression) {

    }

    @Override
    public void visitVoid(BuiltinExpression builtinExpression) {

    }

    @Override
    public void visitVoid(BuiltinStatement builtinStatement) {

    }

    @Override
    public void visitVoid(CallStatement callStatement) {

    }

    @Override
    public void visitVoid(IfStatement ifStatement) {

    }

    @Override
    public void visitVoid(IndexedVariable indexedVariable) {

    }

    @Override
    public void visitVoid(IntLiteral intLiteral) {

    }

    @Override
    public void visitVoid(MainProcedureDeclaration mainProcedure) {

    }

    @Override
    public void visitVoid(LocalBlockStatement localBlockStatement) {

    }

    @Override
    public void visitVoid(LoopStatement loopStatement) {

    }

    @Override
    public void visitVoid(NamedVariable namedVariable) {

    }

    @Override
    public void visitVoid(ProcedureDeclaration procedureDeclaration) {

    }

    @Override
    public void visitVoid(Program program) {

    }

    @Override
    public void visitVoid(SkipStatement skipStatement) {

    }

    @Override
    public void visitVoid(StackNilLiteral stackNilLiteral) {

    }

    @Override
    public void visitVoid(SwapStatement swapStatement) {

    }

    @Override
    public void visitVoid(VariableDeclaration variableDeclaration) {

    }

    @Override
    public void visitVoid(VariableExpression variableExpression) {

    }
}
