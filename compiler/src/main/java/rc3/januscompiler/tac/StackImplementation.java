package rc3.januscompiler.tac;

import rc3.januscompiler.backend.RuntimeAssertion;
import rc3.januscompiler.tac.instances.Instruction;
import rc3.januscompiler.tac.instances.Operand;
import rc3.januscompiler.tac.instances.TemporaryOperand;

public interface StackImplementation {

    /**
     * Generates code to create a new empty stack and returns the {@link Operand} holding the empty stack.
     *
     * @param generator The {@link TacGenerator} instance that accepts the emitted
     *                  {@link Instruction Instructions}.
     * @return An {@link Operand} holding the empty stack.
     */
    Operand generateNil(TacGenerator generator);

    /**
     * Generates code to compare two stacks.
     * <p>
     * If both stacks are equal, a jump to the <code>equalLabel</code> should be made. Otherwise the
     * <code>notEqualLabel</code> should be used as a target.
     * </p>
     *
     * @param generator       The {@link TacGenerator} instance that accepts the emitted
     *                        {@link Instruction Instructions}.
     * @param stackReference1 The first of two stacks to compare.
     * @param stackReference2 The second of two stacks to compare.
     * @param equalLabel      The jump target in case both stacks are equal.
     * @param notEqualLabel   The jump target in case both stacks are <b>NOT</b> equal.
     */
    void generateComparison(TacGenerator generator, Operand stackReference1, Operand stackReference2,
                            String equalLabel, String notEqualLabel);

    /**
     * Generates code to check whether a stack is empty.
     * <p>
     * The default implementation of this method creates an empty stack using
     * {@link StackImplementation#generateNil(TacGenerator)} and compares the
     * supplied stack with it.
     * </p>
     *
     * @param generator      The {@link TacGenerator} instance that accepts the emitted
     *                       {@link Instruction Instructions}.
     * @param stackReference The stack reference which is checked to be empty.
     * @param isEmptyLabel   The jump target in case the stack is empty.
     * @param notEmptyLabel  The jump target in case the stack is <b>NOT</b> empty.
     */
    default void generateIsEmpty(TacGenerator generator, Operand stackReference,
                                 String isEmptyLabel, String notEmptyLabel) {
        Operand nil = generateNil(generator);
        generateComparison(generator, stackReference, nil, isEmptyLabel, notEmptyLabel);
    }

    /**
     * Generates code to initialize a variable with another stack.
     *
     * @param generator      The {@link TacGenerator} instance that accepts the emitted
     *                       {@link Instruction Instructions}.
     * @param targetVariable The address of where to store the assigned-to stack.
     * @param stackReference The reference of the stack that is assigned.
     */
    void generateAssignment(TacGenerator generator, TemporaryOperand targetVariable, Operand stackReference);

    /**
     * Generates code to fetch the top element from a stack.
     * <p>
     * The returned {@link Operand} should hold the value fetched from top of the stack.
     * </p>
     * <p>
     * If the stack is empty, the code generated by this method has to ensure the the code for a failed
     * {@link RuntimeAssertion#TOP_EMPTY_STACK} is executed.
     * </p>
     *
     * @param generator      The {@link TacGenerator} instance that accepts the emitted
     *                       {@link Instruction Instructions}.
     * @param stackReference The stack of which the top value is fetched.
     * @return An {@link Operand} holding the top value.
     */
    Operand generateTop(TacGenerator generator, TemporaryOperand stackReference);

    /**
     * Generates code for a stack push operation.
     * <p>
     * The code generated by this method should place the value passed as the second {@link Operand} on top of
     * the stack, which's memory address is passed as the first {@link Operand}.
     * </p>
     * <p>
     * This method does not have to generate code to clear the contents of the variable pushed onto the stack.
     * </p>
     *
     * @param generator     The {@link TacGenerator} instance that accepts the emitted
     *                      {@link Instruction Instructions}.
     * @param stackVariable The memory address of the stack the value is pushed onto.
     * @param value         The value that is pushed onto the stack.
     */
    void generatePush(TacGenerator generator, TemporaryOperand stackVariable, TemporaryOperand value);

    /**
     * Generates code for a stack pop operation.
     * <p>
     * The code generated by this method should remove the top value from the stack, which's memory address
     * is passed as the first {@link Operand} and store it in the memory address passed as the second {@link Operand}.
     * </p>
     * <p>
     * If the stack is empty, the code generated by this method has to ensure the the code for a failed
     * {@link RuntimeAssertion#POP_EMPTY_STACK} is executed.
     * </p>
     * <p>
     * This method does not have to generate code to ensure that the contents of the variable popped into
     * are zero.
     * </p>
     *
     * @param generator     The {@link TacGenerator} instance that accepts the emitted
     *                      {@link Instruction Instructions}.
     * @param stackVariable The memory address of the stack the value is popped from.
     * @param variable      The memory address the popped value is stored to. This memory address will hold a zero.
     */
    void generatePop(TacGenerator generator, TemporaryOperand stackVariable, TemporaryOperand variable);

    /**
     * Generates the required code when a stack exits a local block and leaves the visible scope.
     *
     * @param generator The {@link TacGenerator} instance that accepts the emitted
     *                  {@link Instruction Instructions}.
     * @param variable  The memory location of the variable holding the stack that left the scope.
     */
    void generateExit(TacGenerator generator, TemporaryOperand variable);

}
