package rc3.lib;

public class NotImplemented extends RuntimeException {
    public NotImplemented() {
        super("Not implemented!");
    }
}
