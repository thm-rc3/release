package rc3.lib;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import static rc3.lib.utils.CollectionUtils.reverse;

/**
 * A utility class to support reversible code generation.
 * <p>
 * Additional to providing an output facility this class supports leveled buffers
 * which can be fetched either forward (similar to a normal buffer) or backward.
 * If a buffer is fetched backwards, its contents are returned in inverse order and
 * every element is inverted using a {@link Function inversion function}.
 * <p>
 * In practice, this class will mostly act as a transparent buffer, only accepting
 * elements. If no buffer is active, these elements are directly passed to an
 * underlying {@link Consumer}. However, if elements are passed to this instance in
 * a reversible context, they will be appended to the buffer and are accessible in
 * reverse order. This also allows stacking of reversible contexts, so that a reversible
 * operation can itself generate elements in reverse, which will result in a portion
 * of non-inverse elements in the result.
 * <p>
 * The default operations are {@link TieredReversibleSink#accept(Object)} and
 * {@link TieredReversibleSink#acceptAll(Iterable)}, which let this sink accept some
 * output (or some iterable of outputs). They are either appended to the currently
 * active buffer or passed to the underlying {@link Consumer}.
 * <p>
 * To let an operation generate elements, the {@link TieredReversibleSink#generate(Runnable)}
 * method is available. The given {@link Runnable} represents an action that modifies this
 * facility via side effects. The similar method {@link TieredReversibleSink#generateReverse(Runnable)}
 * also accepts output from a {@link Runnable} action via side effects, however all elements
 * generated by it are inverted after the action completes and are then passed to the next
 * lower tier of this facility.
 * <p>
 * If you want to explicitly buffer some output, you can use the {@link TieredReversibleSink#generatedBy(Runnable)}
 * and {@link TieredReversibleSink#generatedReverseBy(Runnable)} methods, which return their allocated
 * buffer after the {@link Runnable} action completes, instead of passing the generated elements to the next tier.
 */
public class TieredReversibleSink<E> implements Consumer<E> {
    /**
     * The wrapped output facility. Unbuffered elements are passed to this {@link Consumer}.
     */
    protected final Consumer<E> underlying;
    /**
     * The {@link Function} used to invert single elements.
     */
    protected final Function<? super E, ? extends E> inversionFunction;

    /**
     * A Stack of buffers, which are used to generate output. If this Stack is empty,
     * elements are instead passed to {@link TieredReversibleSink#underlying}.
     */
    protected final Deque<List<E>> openSinks = new ArrayDeque<>();

    /**
     * Creates a new {@link TieredReversibleSink} which allows reversible and classical generation
     * of elements to an output.
     *
     * @param output            The underlying output to which unbuffered elements are transferred.
     * @param inversionFunction A {@link Function} used to invert single elements, when they are
     *                          generated in a reversible context.
     */
    public TieredReversibleSink(Consumer<E> output, Function<? super E, ? extends E> inversionFunction) {
        this.underlying = output;
        this.inversionFunction = inversionFunction;
    }

    protected final Consumer<? super E> activeSink() {
        if (openSinks.isEmpty()) return underlying;
        return openSinks.peekFirst()::add;
    }

    /**
     * Accepts a given element, adding it to the currently open buffer
     * or the underlying {@link Consumer}, if no buffer is open.
     */
    @Override
    public void accept(E element) {
        if (openSinks.isEmpty()) {
            underlying.accept(element);
        } else {
            openSinks.peekFirst().add(element);
        }
    }

    /**
     * Accepts multiple elements, adding them to the currently open buffer
     * or the underlying {@link Consumer}, if no buffer is open.
     * <p>
     * The advantage of using this method over {@link TieredReversibleSink#accept(Object)}
     * is solely for performance reasons when adding multiple elements.
     */
    public void acceptAll(Iterable<? extends E> iterable) {
        final Consumer<? super E> consumer = activeSink();
        iterable.forEach(consumer);
    }

    /**
     * Opens a buffer and returns all elements generated by the given
     * {@link Runnable} action. All elements added until the action completes,
     * are buffered and returned.
     *
     * @param action A {@link Runnable} action to execute, which is allowed to
     *               add elements to this facility.
     * @return All elements added to this facility during execution of the given
     * {@link Runnable} action.
     */
    public List<E> generatedBy(Runnable action) {
        openSinks.push(new ArrayList<>());
        action.run();
        return openSinks.pop();
    }

    /**
     * Opens a buffer and returns all elements generated by the given
     * {@link Runnable} action. All elements added until the action completes,
     * are buffered and returned in reversed order with every single element inverted.
     *
     * @param action A {@link Runnable} action to execute, which is allowed to
     *               add elements to this facility.
     * @return All elements added to this facility during execution of the given
     * {@link Runnable} action in reverse order with every single element inverted.
     */
    public List<E> generatedReverseBy(Runnable action) {
        return generatedBy(() -> generateReverse(action));
    }

    /**
     * Executes the given {@link Runnable} action which is allowed to add
     * elements to this facility.
     *
     * @param action The {@link Runnable} action to execute.
     */
    public void generate(Runnable action) {
        action.run();
    }

    /**
     * Executes the given {@link Runnable} action which is allowed to add
     * elements to this facility. All elements added until this action
     * completes, are then added to the next lower tier present in
     * reverse order with every single element inverted.
     *
     * @param action The {@link Runnable} action to execute.
     */
    public void generateReverse(Runnable action) {
        final Consumer<? super E> consumer = activeSink();
        for (E generated : reverse(generatedBy(action))) {
            consumer.accept(inversionFunction.apply(generated));
        }
    }
}
