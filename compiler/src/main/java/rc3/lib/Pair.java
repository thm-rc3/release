package rc3.lib;

public class Pair<T1, T2>{
    public T1 x;
    public T2 y;

    public Pair (T1 variable, T2 value) {
        this.x = variable;
        this.y = value;
    }
}
